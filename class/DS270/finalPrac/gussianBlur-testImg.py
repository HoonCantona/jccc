#!/usr/bin/env python
# coding: utf-8

# In[1]:


import numpy as np
import skimage
import skimage.color as skic
import skimage.filters as skif
import skimage.data as skid
import skimage.util as sku
import matplotlib.pyplot as plt
get_ipython().run_line_magic('matplotlib', 'inline')


# In[2]:


def show1(img):
    fig, ax = plt.subplots(1, 1, figsize=(8, 8))
    ax.imshow(img, cmap=plt.cm.gray)
    ax.set_axis_off()
    plt.show()


# In[15]:


img = plt.imread("test_image.jpg")
imgBW = skic.rgb2gray(img)
show1(img)
show1(imgBW)


# In[16]:


#img = skic.rgb2gray('test_image.jpg')

#show(img)
show1(skif.gaussian(imgBW, 5.))


# In[17]:


sobimg = skif.sobel(imgBW)
show1(sobimg)


# In[18]:


from ipywidgets import widgets

@widgets.interact(x=(0.01, .2, .005))
def edge(x):
    show1(sobimg < x)


# In[19]:


img_a = skimage.img_as_float(img)

# We take a portion of the image to show the details.
img_a = img[50:200, 150:300]

# We add Gaussian noise.
img_n = sku.random_noise(img)
show1(img_n)


# In[20]:


img_r = skimage.restoration.denoise_tv_bregman(
    img_n, 5.)

fig, (ax1, ax2, ax3) = plt.subplots(
    1, 3, figsize=(12, 8))

ax1.imshow(img_n)
ax1.set_title('With noise')
ax1.set_axis_off()

ax2.imshow(img_r)
ax2.set_title('Denoised')
ax2.set_axis_off()

ax3.imshow(img)
ax3.set_title('Original')
ax3.set_axis_off()


# In[ ]:




