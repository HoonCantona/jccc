from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import statsmodels.formula.api as sm
from sklearn import datasets, linear_model
from matplotlib import cm

# Load the diabetes dataset
boston = datasets.load_boston()

df = pd.DataFrame(boston.data, columns=boston.feature_names)
df['medval'] = pd.DataFrame(boston.target, columns=['Price'])

model = sm.ols(formula='medval ~ AGE + LSTAT', data = df)
fit = model.fit()

fit.summary()

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

x_surf = np.arange(0, 125, 40)                # generate a mesh
y_surf = np.arange(0, 40, 4)
x_surf, y_surf = np.meshgrid(x_surf, y_surf)

exog = pd.core.frame.DataFrame({'AGE': x_surf.ravel(), 'LSTAT': y_surf.ravel()})
out = fit.predict(exog = exog)
ax.plot_surface(x_surf, y_surf,
                out.reshape(x_surf.shape),
                rstride=1,
                cstride=1,
                color='blue',
                alpha = 0.4)

ax.scatter(df['AGE'], df['LSTAT'], df['medval'],
           c='black',
           marker='o',
           alpha=1)

ax.set_xlabel('AGE')
ax.set_ylabel('LSTAT')
ax.set_zlabel('medval')

plt.show()