#For this assignment, we will use a diabetes dataset, which contains diabetes progression data on individuals one year after baseline. Some of the features include Age, various serum levels, BMI. More information can found in their paper (http://web.stanford.edu/~hastie/Papers/LARS/LeastAngle_2002.pdf (Links to an external site.)Links to an external site.). The dataset is located here: http://www4.stat.ncsu.edu/~boos/var.select/diabetes.tab.txt (Links to an external site.)Links to an external site.. It is tab separated so for the separator argument, use '\t' (we used comma separated in class, ',').
#Please answer the following questions, turn in your html and put your answers clearly marked at the bottom of it. Alternatively, you can turn in your answers in a word doc, but include the code separately.

#Using the diabetes dataset, create a linear model to predict the target variable (Y in the data, but really it is a measure of diabetes progression one year after baseline) using Age. Graph your linear model along with the data (label your axes).

#Create a linear model using age, body mass index (BMI), and blood pressure (BP). Predict the diabetes progression for a 45 year old with BMI of 20 and a BP of 100.

#Write down your hypothesis for the linear model from question 2. Specify either your X matrix or what feature each of your x variables represents. Also, specify your exact weights.

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn import datasets, linear_model
from sklearn.metrics import mean_squared_error

diaProg = pd.read_csv('http://www4.stat.ncsu.edu/~boos/var.select/diabetes.tab.txt', sep='\t')
diaProg.head()
#
X = diaProg[['AGE']]
Y = diaProg[['Y']]
myLinModel1 = linear_model.LinearRegression()
myLinModel1.fit(X,Y)
#
y_preds = myLinModel1.predict(X)
MSE = mean_squared_error(Y, y_preds)
print("Mean squared error: %.2f" % MSE)
#
RMSE = np.sqrt(MSE)
print("Root Mean Squared Error: %.2f" % RMSE)
#
A_0 = myLinModel1.intercept_
A_1 = myLinModel1.coef_[0]
print('theta_not:', A_0)
print('theta_one:', A_1)
#
myLinModel1.predict([[45], [20], [100]])
#
plt.scatter(X,Y,color='black')
plt.plot(X,y_preds, color = 'blue', linewidth=3)
plt.xlabel('AGE')
plt.ylabel('Y')
plt.show()
######

X_1 = diaProg[['BMI']]
Y = diaProg[['Y']]
myLinModel2 = linear_model.LinearRegression()
myLinModel2.fit(X_1,Y)
#
y_preds = myLinModel2.predict(X_1)
MSE = mean_squared_error(Y, y_preds)
print("Mean squared error: %.2f" % MSE)
#
RMSE = np.sqrt(MSE)
print("Root Mean Squared Error: %.2f" % RMSE)
#
B_0 = myLinModel2.intercept_
B_1 = myLinModel2.coef_[0]
print('theta_not:', B_0)
print('theta_one:', B_1)
#
myLinModel2.predict([[45], [20], [100]])
#
plt.scatter(X_1,Y,color='black')
plt.plot(X_1,y_preds, color = 'blue', linewidth=3)
plt.xlabel('BMI')
plt.ylabel('Y')
plt.show()
##########

X_2 = diaProg[['BP']]
Y = diaProg[['Y']]
myLinModel3 = linear_model.LinearRegression()
myLinModel3.fit(X_2,Y)
#
y_preds = myLinModel3.predict(X_2)
MSE = mean_squared_error(Y, y_preds)
print("Mean squared error: %.2f" % MSE)
#
RMSE = np.sqrt(MSE)
print("Root Mean Squared Error: %.2f" % RMSE)
#
C_0 = myLinModel3.intercept_
C_1 = myLinModel3.coef_[0]
print('theta_not:', C_0)
print('theta_one:', C_1)
#
myLinModel3.predict([[45], [20], [100]])
#
plt.scatter(X_2,Y,color='black')
plt.plot(X_2,y_preds, color = 'blue', linewidth=3)
plt.xlabel('BP')
plt.ylabel('Y')
plt.show()
#######

X_3 = diaProg[['AGE','BMI','BP']]
Y = diaProg[['Y']]
myLinModel4 = linear_model.LinearRegression()
myLinModel4.fit(X_3,Y)
#
y_preds = myLinModel4.predict(X_3)
MSE = mean_squared_error(Y, y_preds)
print("Mean squared error: %.2f" % MSE)
#
RMSE = np.sqrt(MSE)
print("Root Mean Squared Error: %.2f" % RMSE)
print(myLinModel4.intercept_)
print(myLinModel4.coef_)
#
D_0 = myLinModel4.intercept_
D_1 = myLinModel4.coef_[0]
D_2 = myLinModel4.coef_[1]
D_3 = myLinModel4.coef_[2]
print('theta_not:', D_0)
print('theta_one:', D_1)
print('theta_two:', D_2)
print('theta_three:', D_3)
#
myLinModel4.predict([[45, 20]])
myLinModel4.predict([[45,100]])
#
plt.scatter(X_3,Y,color='black')
plt.plot(X_3,y_preds, color = 'blue', linewidth=3)
plt.xlabel('BP')
plt.ylabel('Y')
plt.show()
#########