
# coding: utf-8

# In[131]:


import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from sklearn import datasets, linear_model
from sklearn.metrics import mean_squared_error

diaProg = pd.read_csv('http://www4.stat.ncsu.edu/~boos/var.select/diabetes.tab.txt', sep='\t')
diaProg.head()


# In[132]:


X = diaProg[['AGE']]
Y = diaProg[['Y']]
myLinModel1 = linear_model.LinearRegression()
myLinModel1.fit(X,Y)


# In[133]:


y_preds = myLinModel1.predict(X)
MSE = mean_squared_error(Y, y_preds)
print("Mean squared error: %.2f" % MSE)


# In[134]:


RMSE = np.sqrt(MSE)
print("Root Mean Squared Error: %.2f" % RMSE)


# In[135]:


A_0 = myLinModel1.intercept_
A_1 = myLinModel1.coef_[0]
print('theta_not:', A_0)
print('theta_one:', A_1)


# In[136]:


myLinModel1.predict([[45], [20], [100]])


# Q1. Answer

# In[137]:


plt.scatter(X,Y,color='black')
plt.plot(X,y_preds, color = 'blue', linewidth=3)
plt.xlabel('AGE')
plt.ylabel('Y')
plt.show()


# Q1. END

# In[138]:


X_3 = diaProg[['AGE','BMI']]
Y = diaProg[['Y']]
myLinModel3 = linear_model.LinearRegression()
myLinModel3.fit(X_3,Y)


# In[139]:


y_preds = myLinModel3.predict(X_3)
MSE = mean_squared_error(Y, y_preds)
print("Mean squared error: %.2f" % MSE)
RMSE = np.sqrt(MSE)
print("Root Mean Squared Error: %.2f" % RMSE)
print(myLinModel3.intercept_)
print(myLinModel3.coef_)


# In[140]:


print('theta_not:', myLinModel3.intercept_)
print('theta_one:', myLinModel3.coef_[0])
#print('theta_two:', myLinModel3.coef_[1])


# In[141]:


print(myLinModel3.predict([[45, 20]]))
print(myLinModel3.predict([[30, 40]]))


# In[142]:


#plt.scatter(X_3,Y,color='black')
#plt.plot(X_3,y_preds, color = 'blue', linewidth=3)
#plt.xlabel('BMI')
#plt.ylabel('Y')
#plt.show()


# In[143]:


X_4 = diaProg[['AGE','BP']]
Y = diaProg[['Y']]
myLinModel4 = linear_model.LinearRegression()
myLinModel4.fit(X_4,Y)


# In[144]:


y_preds = myLinModel4.predict(X_4)
MSE = mean_squared_error(Y, y_preds)
print("Mean squared error: %.2f" % MSE)


# In[145]:


RMSE = np.sqrt(MSE)
print("Root Mean Squared Error: %.2f" % RMSE)
print(myLinModel4.intercept_)
print(myLinModel4.coef_)


# In[146]:


print('theta_not:', myLinModel4.intercept_)
print('theta_one:', myLinModel4.coef_[0])
#print('theta_two:', myLinModel4.coef_[1])


# In[147]:


print(myLinModel4.predict([[45,100]]))
print(myLinModel4.predict([[30,150]]))


# In[148]:


#plt.scatter(X_4,Y,color='black')
#plt.plot(X_4,y_preds, color = 'blue', linewidth=3)
#plt.xlabel('BP')
#plt.ylabel('Y')
#plt.show()


# Q2. Answer
# 
# [age, BMI] = [45, 20] -> 86.88110511
# 
# [age, BP] = [45, 100] -> 163.92899809
# 
# Q2. End
# 
# 
# Q3. Answer
# 
# Hypothesis for age&BMI -> Y = -134.21755079X_0 + 0.48317749X_1 + 9.96778345X_2
# 
# Hypothesis for age&BP -> Y = -85.62865011X_0 + 0.26375705X_1 + 2.37688581X_2
# 
# Specify my weight for age&BMI -> 278.98911191
# 
# Specify my weight for age&BP -> 278.81693286
# 
# Q3. End
