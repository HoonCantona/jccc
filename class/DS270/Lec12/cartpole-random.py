import gym
import numpy as np
import matplotlib.pyplot as plt

def run_episode(env, parameters, visualize):
    observation = env.reset()
    totalreward = 0
    done_counter = 0
    done_break = 40
    for _ in range(200):
        if visualize:
            env.render()
        action = 0 if np.matmul(parameters,observation) < 0 else 1
        observation, reward, done, info = env.step(action)
        totalreward += reward
        if done and (done_counter > done_break):
            break
        elif done:
            done_counter += 1
    return totalreward

def train(submit, visualize=False):
    env = gym.make('CartPole-v0')
    if submit:
        env.monitor.start('cartpole-experiments/', force=True)

    counter = 0
    bestparams = None
    bestreward = 0
    for _ in range(10000):
        counter += 1
        parameters = np.random.rand(4) * 2 - 1
        if visualize:
            print ('episode: {0}'.format(_))
            print ('parameters: {0}'.format(parameters))

        reward = run_episode(env,parameters,visualize)

        if visualize:
            print ('reward: {0} \n'.format(reward))

        if reward > bestreward:
            bestreward = reward
            bestparams = parameters
            if reward == 200:
                if visualize:
                    input("Press Enter to go to next episode")
                env.close()
                break

    if submit:
        for _ in range(100):
            run_episode(env,bestparams)
        env.monitor.close()

    return counter

# train an agent to submit to openai gym
# train(submit=True)

# create graphs
results = []
for _ in range(1000):
    results.append(train(submit=False, visualize = True))

plt.hist(results,50,normed=1, facecolor='g', alpha=0.75)
plt.xlabel('Episodes required to reach 200 reward')
plt.ylabel('Frequency')
plt.title('Histogram of Random Search')
plt.show()

#print(np.sum(results) / 1000.0)
