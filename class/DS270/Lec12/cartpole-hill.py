import gym
import numpy as np
import matplotlib.pyplot as plt

def run_episode(env, parameters, visualize):
    observation = env.reset()
    totalreward = 0
    counter = 0
    done_counter = 0

    if visualize:
        done_break = 40
    else:
        done_break = 0
        
    for _ in range(200):
        if visualize:
            env.render()
        action = 0 if np.matmul(parameters,observation) < 0 else 1
        observation, reward, done, info = env.step(action)
        totalreward += reward
        counter += 1
        if done and (done_counter > done_break):
            break
        elif done:
            done_counter += 1
    return totalreward

def train(submit, visualize=False):
    env = gym.make('CartPole-v0')
    if submit:
        env.monitor.start('cartpole-hill/', force=True)

    max_episodes = 200
    noise_scaling = 0.005
    parameters = np.random.rand(4) * 2 - 1
    bestreward = 0
    counter = 0

    for _ in range(max_episodes):
        counter += 1
        reward = 0
        newparams = parameters + (np.random.rand(4) * 2 - 1)*noise_scaling*(200-reward)

        if visualize:
            print ('episode: {0}'.format(_))
            print ('parameters: {0}'.format(newparams))

        reward = run_episode(env,newparams,visualize)
        # print "reward %d best %d" % (reward, bestreward)
        if visualize:
            print ('reward: {0} \n'.format(reward))

        if reward > bestreward:
            # print "update"
            bestreward = reward
            parameters = newparams
            if reward == 200:
                if visualize:
                    input("Press Enter to go to next episode")
                env.close()
                break

    if submit:
        for _ in xrange(100):
            run_episode(env,parameters)
        env.monitor.close()
    return counter

results = []

for _ in range(1000):
    results.append(train(submit=False, visualize = True))

plt.hist(results,50,normed=1, facecolor='g', alpha=0.75)
plt.xlabel('Episodes required to reach 200 reward')
plt.ylabel('Frequency')
plt.title('Histogram of Hill Search')
plt.show()