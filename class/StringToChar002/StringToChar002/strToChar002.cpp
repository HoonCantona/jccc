/*
Objective
Today we're expanding our knowledge of Strings
and combining it with what we've already learned about loops.
Check out the Tutorial tab for learning materials and an instructional video!

Task
Given a string, S, of length N that is indexed from 0 to N-1,
print its even-indexed and odd-indexed characters as 2
space-separated strings on a single line (see the Sample below for more detail).

Note:
() is considered to be an even index.

Input Format
The first line contains an integer, T (the number of test cases).
Each line i of T the subsequent lines contain a String, S.

Constraints
1 <= T <= 10
2<= length of S <= 10000

Output Format
For each String S_j (where 0 <= j <= T-1),
print S_j's even-indexed characters, followed by a space, followed by
S_j's odd-indexed characters.

Sample Input
2
Hacker
Rank

Sample Output
Hce akr
Rn ak
*/
#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;

class GamePlay
{
public:
	GamePlay();
	~GamePlay();
	int testingNum();
	void getWord(char n);
	void splitWord(char n);
	void reorgWord(char n);
	void countWord(char word);

private:
	char word[];
	string S;
	const int testingMin = 1;
	const int testingMax = 2;
	const int maxWord = 10000;
	const int minWord = 2;
};

GamePlay::GamePlay() {
	;
}

GamePlay::~GamePlay() {
	;
}

int main() {
	/* Enter your code here. Read input from STDIN. Print output to STDOUT */


	return 0;
}

int testingNum() {
	int n;
	cin >> n;
	return n;
}

void getWord(int n, char word) {
	for (int i = 0; i < testingNum(); i++) {
		splitWord(word);
	}
}

void countWord(char word) {
	;
}

void splitWord(char word) {
	for (int i = 0; i < word.size(); i++)
	{
		if (i % 2 == 0) {
			string goLeft = word[i];
		}
		else {
			string goRight = word[i];
		}
	}
}

void reorgWord(string goLeft, string goRight) {
	cout << goLeft << "  " << goRight << endl;
}