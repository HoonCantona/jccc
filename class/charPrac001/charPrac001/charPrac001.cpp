#include <iostream>
#include <vector>

using namespace std;
char display(char word[]);
int main()
{
	int num;
	int i = 0;
	cout << "Enter a positive integer: \n";
	cin >> num;
	while (i < num)
	{
		char myChar[10];
		char goLeft[5];
		char goRight[5];
		cout << "Enter a word for char: ";
		cin >> myChar;
		cin.ignore();
		int j = 0;
		int countLeft = 0;
		int countRight = 0;
		while(myChar[j] != '\0')
		{
			if (j % 2 == 0)
			{
				goLeft[countLeft] = myChar[j];
				countLeft++;
			}
			else
			{
				goRight[countRight] = myChar[j];
				countRight++;
			}
			//cout << myChar << endl;
			j++;
		}
		cout << display(goLeft) << "  " << display(goRight) << endl;
		/*string myStr;
		cout << "Enter a word for str: ";
		cout << myStr;*/
		i++;
	}
	

	return 0;
}

char display(char word[])
{
	int i = 0;
	char s;
	while (word[i] != '\0')
	{
		s = s + word[i];
		i++;
	}
	return s;
}