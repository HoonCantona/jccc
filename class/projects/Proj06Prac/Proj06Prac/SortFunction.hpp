#ifndef _SORT_FUNCTION_HPP
#define _SORT_FUNCTION_HPP

#include <iostream>
#include <vector>
using namespace std;
#include "DataEntry_dHs.hpp"

void SelectionSort(vector<DataEntry>& data, const string& onKey);
void insertionSort(vector<DataEntry>& data, const string& onKey);
void bubbleSort(vector<DataEntry>& data, const string& onKey);
#endif // !_SORT_FUNCTION_HPP
