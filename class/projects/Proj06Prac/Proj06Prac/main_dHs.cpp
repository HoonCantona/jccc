#include <iostream>
#include <vector>
using namespace std;

#include "DataEntry_dHs.hpp"
#include "SortFunction.hpp"
#include "Timer.hpp"
#include "Menu.hpp"

int main()
{
	Menu::Header("U.S. Chronic Disease Indicators");
	vector<string> filenames = { "100_US_Chronic_Disease_Indicators.csv",
		"1000_US_Chronic_Disease_Indicators.csv",
		"10000_US_Chronic_Disease_Indicators.csv",
		"523487_US_Chronic_Disease_Indicators.csv" };
	vector<string> sorts = { "Selection Sort", "Insertion Sort", "Bubble Sort" };
	vector<string> columns = 
	{ "YearStart", "YearEnd", "LocationAbbr", "LocationDesc", "Topic", "Question" };
	cout << "Which file do you want to load?" << endl;
	int fileChoice = Menu::ShowIntMenuWithPrompt(filenames);
	cout << "Which sort do you want to use?" << endl;
	int sortChoice = Menu::ShowIntMenuWithPrompt(sorts);
	cout << "Which column do you want to sort on?" << endl;
	int sortOnChoice = Menu::ShowIntMenuWithPrompt(columns);
	Timer timer;
	vector<DataEntry> data;
	string filename = filenames[fileChoice - 1];
	cout << left << setw(15) << "BEGIN:" << "Loading data from file, \"" << filename << "\"..." << endl;
	timer.Start();
	ReadData(data, filename);
	cout << left << setw(15) << "COMPLETED:" << "In " << timer.GetElapsedMilliseconds() << " milliseconds"
		<< endl << endl;
	cout << data.size() << " item loaded" << endl;
	cout << left << setw(15) << "BEGIN:" << "Sorting data with " << sorts[sortChoice - 1] << "..." << endl;
	timer.Start();
	if (sortChoice == 1)
	{
		SelectionSort(data, columns[sortOnChoice - 1]);
	}
	else if (sortChoice == 2)
	{
		insertionSort(data, columns[sortOnChoice - 1]);
	}
	else if (sortChoice == 3)
	{
		bubbleSort(data, columns[sortOnChoice - 1]);
	}
	cout << left << setw(15) << "COMPLETED:" << "In " << timer.GetElapsedMilliseconds() << " milliseconds"
		<< endl << endl;
	cout << endl << "Writing list out to \"output.txt\"..." << endl;
	ofstream output("output.txt");
	for (map<string, string>::iterator it = data[0].fields.begin(); it != data[0].fields.end(); it++)
	{
		output << left << setw(30) << it->first;
	}
	output << endl;
	for (unsigned int i = 0; i < data.size(); i++)
	{
		data[i].Output(output);
	}
	output.close();
	Menu::Pause();
	return 0;
}