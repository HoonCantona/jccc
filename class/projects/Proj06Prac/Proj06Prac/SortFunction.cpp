#include "SortFunction.hpp"

void SelectionSort(vector<DataEntry>& data, const string& onKey)
{
	int n = data.size();
	for (int j = 0; j < n - 1; j++)
	{
		int iMin = j;
		for (int i = j + 1; i < n; i++)
		{
			if (data[i].fields[onKey] < data[iMin].fields[onKey])
			{
				iMin = i;
			}
			if (iMin != j)
			{
				DataEntry temp = data[j];
				data[j] = data[iMin];
				data[iMin] = temp;
			}
		}
	}
}

void insertionSort(vector<DataEntry>& data, const string& onKey)
{
	int n = data.size();
	for (int unsorted = 1; unsorted < n; unsorted++)
	{
		DataEntry nextItem = data[unsorted];
		int loc = unsorted;
		while ((loc > 0) && (data[loc - 1].fields > nextItem.fields))
		{
			data[loc] = data[loc - 1];
			loc--;
		}
		data[loc] = nextItem;
	}
}

void bubbleSort(vector<DataEntry>& data, const string& onKey)
{
	int n = data.size();
	bool sorted = false;
	int pass = 1;
	while (!sorted && (pass < n))
	{
		sorted = true;
		for (int index = 0; index < n - pass; index++)
		{
			int nextIndex = index + 1;
			if (data[index].fields > data[nextIndex].fields)
			{
				std::swap(data[index], data[nextIndex]);
				sorted = false;
			}
		}
		pass++;
	}
}