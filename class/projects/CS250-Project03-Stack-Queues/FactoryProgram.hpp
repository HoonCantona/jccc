#ifndef FACTORY_PROGRAM_HPP
#define FACTORY_PROGRAM_HPP

#include "UTILITIES/Menu.hpp"
#include "UTILITIES/Logger.hpp"

#include "DATASTRUCTURES/Queue.hpp"
#include "DATASTRUCTURES/Stack.hpp"
#include "DATASTRUCTURES/LinkedList.hpp"

#include <string>
#include <fstream>
using namespace std;

typedef string Ingredient;
typedef string Burger;

class FactoryProgram
{
    public:
    FactoryProgram();
    ~FactoryProgram();

    //! Runs the main program, calling the three steps
    void Run();

    //! Loads the queue of ingredients from a text file
    void LoadIngredients();
    //! Works in a loop to assemble the burgers
    void AssembleBurgers();
    //! Outputs a queue of completed burgers to a text file
    void DeliverBurgers();

    private:
    //! Checks if ingredient given is on the safe-list
    bool IsIngredientAcceptable( const Ingredient& ingredient ) const;
    //! Check if a burger has any low-quality ingredients (not on safe-list)
    void CheckBurgerQuality();
    //! Check if  burger is completed
    void CheckBurgerCompletion();

    //! Put an ingredient onto the burger
    void PutIngredientOnBurger( Burger& burger, const Ingredient& ingredient );

    //! Queue of incoming ingredients
    Queue<Ingredient>           m_ingredientConveyorBelt;

    //! Stack of ingredients being built into a burger
    Stack<Ingredient>           m_burgerAssembler;

    //! Queue of finished burgers
    Queue<Burger>               m_assembledBurgersConveyorBelt;

    //! List of acceptable ingredients
    LinkedList<Ingredient>		m_acceptableIngredients;

    //! File to output the factory's messages
    ofstream                    m_factoryOutput;
};

FactoryProgram::FactoryProgram()
{
    // Acceptable ingredient list
    m_acceptableIngredients.PushBack(  "BUNBOTTOM"  );
    m_acceptableIngredients.PushBack(  "BUNTOP"  );
    m_acceptableIngredients.PushBack(  "LETTUCE"  );
    m_acceptableIngredients.PushBack(  "TOMATO"  );
    m_acceptableIngredients.PushBack(  "CHICKEN" );
    m_acceptableIngredients.PushBack(  "MAYO"  );
    m_acceptableIngredients.PushBack(  "BEANPATTY"  );
    m_acceptableIngredients.PushBack(  "HAM"  );
    m_acceptableIngredients.PushBack(  "EGG"  );
    m_acceptableIngredients.PushBack(  "CHEESE"  );
    m_acceptableIngredients.PushBack(  "MUSTARD"  );

    // Initialize the program output file
    m_factoryOutput.open( "OUTPUT-factory.txt" );
}

FactoryProgram::~FactoryProgram()
{
    // Close the program output file
    m_factoryOutput.close();
}

void FactoryProgram::Run()
{
    Logger::Out( "Function Begin", "FactoryProgram::Run" );

    Menu::ClearScreen();

    m_factoryOutput << "Starting factory..." << endl;
    m_factoryOutput << "Toot toot, factory running!" << endl << endl;

    LoadIngredients();
    AssembleBurgers();
    DeliverBurgers();
}

void FactoryProgram::LoadIngredients()
{
    Logger::Out( "Function Begin", "FactoryProgram::LoadIngredients" );

    m_factoryOutput << "Loading ingredients from INPUT-ingredients-list.txt..." << endl;

    ifstream input( "INPUT-ingredient-list.txt" );
    Ingredient buffer;

    while ( input >> buffer )
    {
        // Add this next Ingredient onto the conveyor belt m_ingredientConveyorBelt
        //cout << "FactoryProgram::LoadIngredients - NOT IMPLEMENTED" << endl;
		m_ingredientConveyorBelt.Push(buffer);
    }

    m_factoryOutput << "\t" << m_ingredientConveyorBelt.Size() << " ingredients loaded" << endl << endl;
}

void FactoryProgram::AssembleBurgers()
{
    Logger::Out( "Function Begin", "FactoryProgram::AssembleBurgers" );

    m_factoryOutput << "Assembling burgers..." << endl;

    // Process the queue of items on the m_ingredientConveyorBelt
    // While there's still something on the conveyor belt...
    //  - Put the front-most item from the conveyor belt onto the burger building stack m_burgerAssembler
    //  - Pop the front-most item off the input queue
    //  - Call CheckBurgerQuality()
    //  - Call CheckBurgerCompletion()

    //cout << "FactoryProgram::AssembleBurgers - NOT IMPLEMENTED" << endl;
	while (!m_ingredientConveyorBelt.IsEmpty())
	{
		m_burgerAssembler.Push(m_ingredientConveyorBelt.Front());
		m_ingredientConveyorBelt.Pop();
		CheckBurgerQuality();
		CheckBurgerCompletion();
	}
}

bool FactoryProgram::IsIngredientAcceptable( const Ingredient& ingredient ) const
{
    Logger::Out( "Function Begin", "FactoryProgram::IsIngredientAcceptable" );

    // Check if the ingredent given is in the list of acceptable ingredients.
    // If so, return true. Otherwise, return false.
    //cout << "FactoryProgram::IsIngredientAcceptable - NOT IMPLEMENTED" << endl;
    //return false; // temporary
	if (m_acceptableIngredients.Contains(ingredient))
	{
		return true;
	}
	else
	{
		return false;
	}
}

void FactoryProgram::CheckBurgerQuality()
{
    Logger::Out( "Function Begin", "FactoryProgram::CheckBurgerQuality" );

    m_factoryOutput << "\t CHECKING BURGER QUALITY... ";
    // If the top-most item on the burger stack is an invalid ingredient, remove it from the m_burgerAssembler.

    //cout << "FactoryProgram::CheckBurgerQuality - NOT IMPLEMENTED" << endl;
	if (!m_acceptableIngredients.Contains(m_burgerAssembler.Top()))
	{
		m_burgerAssembler.Pop();
	}

    m_factoryOutput << endl;
}

void FactoryProgram::CheckBurgerCompletion()
{
    Logger::Out( "Function Begin", "FactoryProgram::CheckBurgerCompletion" );

    m_factoryOutput << "\t CHECKING BURGER COMPLETION... ";

    // If we've encountered the end of a burger (the BUNTOP), then...
    //  - Create a new Burger item.
    //  - While the burger assembler stack is not empty...
    //      * Take the top item and put it on the burger. Use PutIngredientOnBurger( newBurger, m_burgerAssembler.Top() )
    //      * Pop the item from the top of the burger assembler stack.
    //  - Afterwards, push the new burger item onto the m_assembledBurgersConveyorBelt queue.

    //cout << "FactoryProgram::CheckBurgerCompletion - NOT IMPLEMENTED" << endl;
	
	Burger newBurger;
	if (m_burgerAssembler.Top() == "BUNTOP")
	{
		while (!m_burgerAssembler.IsEmpty())
		{
			PutIngredientOnBurger(newBurger, m_burgerAssembler.Top());
			m_burgerAssembler.Pop();
		}
	}
	
	
	m_assembledBurgersConveyorBelt.Push(newBurger);

    m_factoryOutput << endl;
}

void FactoryProgram::PutIngredientOnBurger( Burger& burger, const Ingredient& ingredient )
{
    // Add the ingredient onto the burger
    burger += ingredient + "\n";
}

void FactoryProgram::DeliverBurgers()
{
    Logger::Out( "Function Begin", "FactoryProgram::DeliverBurgers" );

    m_factoryOutput << "Delivering burgers to OUTPUT-burgers.txt..." << endl;

    ofstream output( "OUTPUT-burgers.txt" );

    // Output the completed burgers to the output file
    // While the burger output queue (m_assembledBurgersConveyorBelt) is not empty...
    //  - Output the front-most item to the text file.
    //  - Remove the front-most item from the queue.

	while (!m_assembledBurgersConveyorBelt.IsEmpty())
	{
		output << m_assembledBurgersConveyorBelt.Front() << endl;
		m_assembledBurgersConveyorBelt.Pop();
	}

    //cout << "FactoryProgram::DeliverBurgers - NOT IMPLEMENTED" << endl;

    cout << "\t Burgers saved to OUTPUT-burgers.txt" << endl;

    output.close();
}

#endif
