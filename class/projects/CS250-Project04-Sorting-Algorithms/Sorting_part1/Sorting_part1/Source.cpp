#include <iostream>
using namespace std;

int main()
{
	int arr[] = { 1, 2, 3 };
	cout << arr[0] << "\t" << arr[1] << endl;
	arr[1] = arr[0];
	cout << arr[0] << "\t" << arr[1] << endl;
	arr[0] = arr[1];
	cout << arr[0] << "\t" << arr[1] << endl;
	int a = 1 / 2;
	cout << a << endl;
	return 0;
}