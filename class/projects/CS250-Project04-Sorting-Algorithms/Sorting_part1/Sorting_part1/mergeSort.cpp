template<class ItemType>
void merge(ItemType theArray[], int first, int mid, int last)
{
	ItemType tempArray[MAX_SIZE];
	int first1 = first, last1 = mid, first2 = mid + 1, last2 = last;
	int index = first1;
	while ((first1 <= last1) && (first2 <= last2))
	{
		if (theArray[first1] <= theArray[first2])
		{
			tempArray[index] = theArray[first1];
			first1++;
		}
		else
		{
			tempArray[index] = theArray[first2];
			first2++;
		}
		index++;
	}
	while (first1 <= last1)
		tempArray[index] = theArray[first1]; first1++; index++;
	while (first2 <= last2)
		tempArray[index] = theArray[first2]; first2++; index++;
	for (index = first; index <= last; index++)
		theArray[index] = tempArray[index];
}
template<class ItemType>
void mergeSort(ItemType theArray[], int first, int last)
{
	if (first < last)
	{
		int mid = first + (last - first) / 2;
		mergeSort(theArray, first, mid);
		mergeSort(theArray, mid + 1, last);
		merge(theArray, first, mid, last);
	}
}
/*
mergeSort(   )|||start mergeSort()
mergeSort(   )|||theArray: {"C", "A", "T"}, first: 0, last: 2
mergeSort(   )|||	if statement entry		first: 0, last: 2
mergeSort(   )|||	first < last (true statement)
mergeSort(   )|||	start if statement		first: 0, last: 2
mergeSort(   )|||		set a int variable named mid as first + (last - first) / 2		mid: 0 + (2 - 0)/2 = 1
mergeSort(1st)|||		start mergeSort()
mergeSort(1st)|||		theArray: {"C", "A", "T"}, first: 0, mid: 1
mergeSort(1st)|||			if statement entry		first: 0, last: 1 (from the parameters right above)
mergeSort(1st)|||			first < last (true statement)
mergeSort(1st)|||			start if statement		first: 0, last: 1
mergeSort(1st)|||				set a int variable named mid as first + (last - first) / 2		mid: 0 + (1 - 0)/2 = 0 (overwrite the variable)
mergeSort(1st)|||				start mergeSort()
mergeSort(1st)|||				theArray: {"C", "A", "T"}, first: 0, mid: 0
mergeSort(1st)|||					if statement entry		first: 0, last: 0 (from the parameters right above)
mergeSort(1st)|||					first < last (false statement)
mergeSort(1st)|||					skip if statement
mergeSort(1st)|||				mergeSort() done
mergeSort(1st)|||				start mergeSort()		mid: 0, mid + 1: 0 + 1 = 1
mergeSort(1st)|||				theArray: {"C", "A", "T"}, mid + 1: 1, last: 1
mergeSort(1st)|||					start if statement		first: 1, last: 1 (from the parameters right above)
mergeSort(1st)|||					first < last (false statement)
mergeSort(1st)|||					skip if statement
mergeSort(1st)|||				mergeSort() done
mergeSort(1st)|||				start merge()
mergeSort(1st)|||				theArray: {"C", "A", "T"}, first: 0, mid: 0, last: 1
mergeSort(1st)|||					declare ItemType named tempArray, the size is MAX_SIZE
mergeSort(1st)|||					set a variable named first1 as first		first1: 0
mergeSort(1st)|||					set a variable named last1 as mid		last1: 0
mergeSort(1st)|||					set a variable named first2 as mid + 1		first2: 1
mergeSort(1st)|||					set a variable named last1 as last		last2: 1
mergeSort(1st)|||					set a variable named index as first1		index: 0
mergeSort(1st)|||					while loop entry		first1: 0, last1: 0, first2: 1, last2: 1
mergeSort(1st)|||					(first1 <= last1) && (first2 <= last2) (true statement)
mergeSort(1st)|||					start while loop
mergeSort(1st)|||						if statement entry		theArray[first1]: "C", theArray[first2]: "A"
mergeSort(1st)|||						theArray[first1] <= theArray[first2] (false statement)
mergeSort(1st)|||						skip if statement
mergeSort(1st)|||						start else statement		index: 0, first2: 1, theArray[first2]: "A"
mergeSort(1st)|||							set tempArray[index] as theArray[first2]		tempArray[index]: "A", tempArray: {"A"}
mergeSort(1st)|||							first2: 1 + 1 = 2 (increase the variable)
mergeSort(1st)|||						else statement done
mergeSort(1st)|||						index: 0 + 1 = 1 (increase the variable)
mergeSort(1st)|||					while loop entry		first1: 0, last1: 0, first2: 2, last2: 1
mergeSort(1st)|||					(first1 <= last1) && (first2 <= last2) (false statement)
mergeSort(1st)|||					while loop done
mergeSort(1st)|||					while loop entry		first1: 0, last1: 0
mergeSort(1st)|||					first1 <= last1 (true statement)
mergeSort(1st)|||					while loop start		index: 1, first1: 0, theArray[first1]: "C"
mergeSort(1st)|||						set tempArray[index] as theArray[first1]		tempArray[index]: "C", tempArray: {"A", "C"}
mergeSort(1st)|||						first1: 0 + 1 = 1 (increase the variable)
mergeSort(1st)|||						index: 1 + 1 = 2 (increase the variable)
mergeSort(1st)|||					while loop entry		first1: 1, last1: 0
mergeSort(1st)|||					first1 <= last1 (false statement)
mergeSort(1st)|||					while loop done
mergeSort(1st)|||					while loop entry		first2: 2, last2: 1
mergeSort(1st)|||					first2 <= last2 (false statement)
mergeSort(1st)|||					while loop done		index: 2, first: 0, last: 1
mergeSort(1st)|||					for loop entry		index: 0 (overwrite the variable), last: 1
mergeSort(1st)|||					index <= last (true statement)
mergeSort(1st)|||					for loop start		index: 0, theArray[index]: "C", tempArray[index]: "A"
mergeSort(1st)|||						set theArray[index] as tempArray[index]		theArray: {"A", "A", "T"} (overwrite theArray[0])
mergeSort(1st)|||						index: 0 + 1 = 1 (increase the variable)
mergeSort(1st)|||					for loop entry		index: 1, last: 1
mergeSort(1st)|||					index <= last (true statement)
mergeSort(1st)|||					for loop start		index: 1, theArray[index]: "A", tempArray[index]: "C"
mergeSort(1st)|||						set theArray[index] as tempArray[index]		theArray: {"A", "C", "T"} (overwrite theArray[1])
mergeSort(1st)|||						index: 1 + 1 = 2 (increase the variable)
mergeSort(1st)|||					for loop entry		index: 2, last: 1
mergeSort(1st)|||					index <= last (false statement)
mergeSort(1st)|||					for loop done
mergeSort(1st)|||				merge() done
mergeSort(1st)|||			if statement done
mergeSort(1st)|||		mergeSort() done
mergeSort(2nd)|||		start mergeSort()		mid: 0, mid + 1: 1, last: 1
mergeSort(2nd)|||		theArray: {"A", "C", "T"}, first: 1, last: 1 (from the parameters right above)
mergeSort(2nd)|||			if statement entry		first: 1, last: 1
mergeSort(2nd)|||			first < last (false statement)
mergeSort(2nd)|||			skip if statement
mergeSort(2nd)|||		mergeSort() done
merge(       )|||		Start merge()		first: 1, mid: 0, last: 1
merge(       )|||		theArray: {"A", "C", "T"}, first: 1, mid: 0, last: 1 (from the parameters right above)
merge(       )|||			declare ItemType named tempArray, the size is MAX_SIZE
merge(       )|||			set a variable named first1 as first		first1: 1
merge(       )|||			set a variable named last1 as mid		last1: 0
merge(       )|||			set a variable named first2 as mid + 1		first2: 1
merge(       )|||			set a variable named last1 as last		last2: 1
merge(       )|||			set a variable named index as first1		index: 1
merge(       )|||			while loop entry		first1: 1, last1: 0, first2: 1, last2: 1
merge(       )|||			(first1 <= last1) && (first2 <= last2) (false statement)
merge(       )|||			while loop done
merge(       )|||			while loop entry		first1: 1, last1: 0
merge(       )|||			first1 <= last1 (false statement)
merge(       )|||			while loop done
merge(       )|||			while loop entry		first2: 1, last2: 1
merge(       )|||			first1 <= last1 (true statement)
merge(       )|||			start while loop		index: 1, theArray[first2]: "C"
merge(       )|||				set tempArray[index] as theArray[first2]		tempArray[index]: "C"
merge(       )|||				first2: 1 + 1 = 2 (increase the variable)
merge(       )|||				index: 1 + 1 = 2 (increase the variable)
merge(       )|||			while loop entry		first2: 2, last2: 1
merge(       )|||			first1 <= last1 (false statement)
merge(       )|||			while loop done		index: 2, first: 1, last: 1
merge(       )|||			for loop entry		index: 1 (overwrite the variable), last: 1
merge(       )|||			index <= last (true statement)
merge(       )|||			start for loop
merge(       )|||				set theArray[index] as tempArray[index]			theArray[index]: "C"
merge(       )|||				theArray: {"A", "C", "T"} (there is no change, overwrite the same value)
merge(       )|||				index: 1 + 1 = 2 (increase the variable)
merge(       )|||			for loop entry		index: 2, last: 1
merge(       )|||			index <= last (false statement)
merge(       )|||			for loop done
merge(       )|||		merge() done
mergeSort(   )|||	if statement done
mergeSort(   )|||mergeSort() done
*/