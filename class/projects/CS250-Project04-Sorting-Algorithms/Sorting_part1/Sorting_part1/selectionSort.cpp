template<class ItemType>
int findIndexOfLargest(const ItemType theArray[], int size)
{
	int indexSoFar = 0;
	for (int currentIndex = 1; currentIndex < size; currentIndex++)
	{
		if (theArray[currentIndex] > theArray[indexSoFar])
			indexSoFar = currentIndex;
	}
	return indexSoFar;
}
template<class ItemType>
void selectionSort(ItemType theArray[], int n)
{
	for (int last = n 1;
		last >= 1; last)
	{
		int largest = findIndexOfLargest(theArray, last + 1);
		std::swap(theArray[largest], theArray[last]);
	}
}
/*
selectionSort() | n: 3 theArray: A, C, T
selectionSort() | for (int last = n 1;
last >= 1; last)
selectionSort() | For loop entry last: 2
findIndexOfLargest() | size: 3 theArray: A, C, T
findIndexOfLargest() | FOR (int currentIndex = 1; currentIndex < size; currentIndex++)
findIndexOfLargest() | For loop entry currentIndex: 1 indexSoFar: 0
findIndexOfLargest() | theArray[currentIndex] > theArray[indexSoFar]
findIndexOfLargest() | set indexSoFar to currentIndex (0).
findIndexOfLargest() | For loop entry currentIndex: 2 indexSoFar: 1
findIndexOfLargest() | theArray[currentIndex] > theArray[indexSoFar]
findIndexOfLargest() | set indexSoFar to currentIndex (1).
findIndexOfLargest() | FOR LOOP DONE
findIndexOfLargest() | Return indexSoFar: 2
selectionSort() | largest: 2
selectionSort() | Swap 2 and 2
selectionSort() | theArray: A, C, T
selectionSort() | For loop entry last: 1
findIndexOfLargest() | size: 2 theArray: A, C
findIndexOfLargest() | FOR (int currentIndex = 1; currentIndex < size; currentIndex++)
findIndexOfLargest() | For loop entry currentIndex: 1 indexSoFar: 0
findIndexOfLargest() | theArray[currentIndex] > theArray[indexSoFar]
findIndexOfLargest() | set indexSoFar to currentIndex (0).
findIndexOfLargest() | FOR LOOP DONE
findIndexOfLargest() | Return indexSoFar: 1
selectionSort() | largest: 1
selectionSort() | Swap 1 and 1
selectionSort() | theArray: A, C, T
selectionSort() | For loop done
selectionSort() | Sort complete.
*/