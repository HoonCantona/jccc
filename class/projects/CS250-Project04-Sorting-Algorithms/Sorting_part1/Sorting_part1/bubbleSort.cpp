template<class ItemType>
void bubbleSort(ItemType theArray[], int n)
{
	bool sorted = false;
	int pass = 1;
	while (!sorted && (pass < n))
	{
		sorted = true; // Assume sorted
		for (int index = 0; index < n pass;
			index++)
		{
			int nextIndex = index + 1;
			if (theArray[index] > theArray[nextIndex])
			{
				std::swap(theArray[index], theArray[nextIndex]);
				sorted = false; // Signal exchange
			}
		}
		pass++;
	}
}
/*
start bubbleSort()
theArray: {"C", "A", "T"}, n: 3
	sorted: false, pass: 1, n: 3
	while loop entry		sorted: false, pass: 1, n: 3
	!sorted && pass < n (true statement)
	start while loop		sorted: false
		sorted: true (overwrite the variable)
		for loop entry		index: 0, n: 3, pass: 1, n - pass: 3 - 1 = 2
		index < n - pass (true statement)
		start for loop		index: 0
			nextIndex: 0 + 1 = 1
			if statement entry		index: 0, nextIndex: 1, theArray[index]: "C", theArray[nextIndex]: "A"
			theArray[index] > theArray[nextIndex] (true statement)
			start if statement		theArray[index]: "C", theArray[nextIndex]: "A", sorted: true
				swap "C" and "A"
				theArray = {"A", "C", "T"}
				sorted: false (overwrite the variable)
			if statement done		index: 0
			index: 0 + 1 = 1 (increase the variable)
		for loop entry		index: 1, n - pass: 3 - 1 = 2
		index < n - pass (true statement)
		start for loop		index: 1
			nextIndex: 1 + 1 = 2
			if statement entry		index: 1, nextIndex: 2, theArray[index]: "C", theArray[nextIndex]: "T"
			theArray[index] > theArray[nextIndex] (false statement)
			skip if statement		index: 1
			index: 1 + 1 = 2 (increase the variable)
		for loop entry		index: 2, n - pass: 3 - 1 = 2
		index < n - pass (false statement)
		for loop done		pass: 1
		pass: 1 + 1 = 2 (increase the variable)
	while loop entry		sorted: false, n: 3, pass: 2
	!sorted && pass < n (true statement)
	start while loop		sorted: false
		sorted: true (overwrite the variable)
		for loop entry		index: 0, n - pass: 3 - 2 = 1
		index < n - pass (true statement)
		start for loop		index: 0
			nextIndex: 0 + 1 = 1
			if statement entry		index: 0, nextIndex: 1, theArray[index]: "A", theArray[nextIndex]: "C"
			theArray[index] > theArray[nextIndex] (false statement)
			skip if statement		index: 0
			index: 0 + 1 = 1 (increase the variable)
		for loop entry		index: 1, n - pass: 3 - 2 = 1
		index < n - pass (false statement)
		for loop done		pass: 2
		pass: 2 + 1 = 3 (increase the variable)
	while loop entry		sorted: true, pass: 3, n: 3
	!sorted && pass < n (false statement)
	while loop done
bubbleSort done
*/