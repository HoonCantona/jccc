template<class ItemType>
void insertionSort(ItemType theArray[], int n)
{
	for (int unsorted = 1; unsorted < n; unsorted++)
	{
		ItemType nextItem = theArray[unsorted];
		int loc = unsorted;
		while ((loc > 0) && (theArray[loc 1]
> nextItem))
		{
			theArray[loc] = theArray[loc 1];
			loc;
		}
		theArray[loc] = nextItem;
	}
}
/*
start insertionSort
theArray: {"C", "A", "T"}, n: 3
	for loop entry		unsorted: 1, n: 3
	unsorted < n (true statement)
	start for loop		unsorted: 1, theArray[unsorted]: "A"
		set a ItemType named nextItem as theArray[unsorted]		nextItem: "A"
		set a int variable named loc as unsorted		loc: 1
		while loop entry		loc - 1: 1 - 1 = 0, theArray[loc - 1]: "C", nextItem: "A"
		(loc > 0) && (theArray[loc - 1] > nextItem) (true statement)
		start while loop		loc: 1, theArray[loc]: "A", theArray[loc - 1]: "C"
			set theArray[loc] as theArray[loc - 1]		theArray: {"C", "C", "T"}
			loc: 1 - 1 = 0 (decrease the variable)
		while loop entry		loc - 1: 0 - 1 = -1, theArray[loc - 1]: undefiend, nextItem: "A"
		(loc > 0) && (theArray[loc - 1] > nextItem) (false statement)
		while loop done		loc: 0, theArray[loc]: "C", nextItem: "A"
		set	theArray[loc] as nextItem		theArray: {"A", "C", "T"}, unsorted: 1
		unsorted: 1 + 1 = 2
	for loop entry		unsorted: 2, n: 3
	unsorted < n (true statement)
	start for loop		unsorted: 2, theArray[unsorted]: "T"
		set a ItemType named nextItem as theArray[unsorted]		nextItem: "T"
		set a int variable named loc as unsorted		loc: 2
		while loop entry		loc - 1: 2 - 1 = 1, theArray[loc - 1]: "C", nextItem: "T"
		(loc > 0) && (theArray[loc - 1] > nextItem) (false statement)
		while loop done		loc: 2, theArray[loc]: "T", nextItem: "T"
		set	theArray[loc] as nextItem		unsorted: 2
		unsorted: 2 + 1 = 3
	for loop entry		unsorted: 3, n: 3
	unsorted < n (false statement)
	for loop done
insertionSort() done
*/