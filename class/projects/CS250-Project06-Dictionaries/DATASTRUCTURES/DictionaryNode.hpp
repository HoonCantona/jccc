#ifndef DICTIONARY_NODE_HPP
#define DICTIONARY_NODE_HPP

template <typename TK, typename TV>
struct DictionaryNode
{
    //! Initialize the node as unused
    DictionaryNode() { this->used = false; }

    //! Initialize the node as used, setting its key and value
    DictionaryNode( TK key, TV value )
    {
        this->key = key;
        this->value = value;
        this->used = true;
    }

    //! The key of the node
    TK key;
    //! The data that the node is storing
    TV value;
    //! Whether this node is currently in use; false for not in use.
    bool used;
};

#endif
