#ifndef _BINARY_SEARCH_TREE_HPP
#define _BINARY_SEARCH_TREE_HPP
#include <iostream>
#include <string>
#include <sstream>
using namespace std;
#include "Node.hpp"
#include "ExceptionNotImplemented.hpp"

template <typename TK, typename TD>
class BinarySearchTree
{
public:
	BinarySearchTree();
	~BinarySearchTree();
	void Push(const TK& newKey, const TD& newData);
	void Delete(const TK& key);
	bool Contains(const TK& key);
	bool RecursiveContains(const TK& key, Node<TK, TD>* ptrCurrent);
	TD* GetData(const TK& key);
	Node<TK, TD>* FindNode(const TK& key);

	string GetInOrder();
	string GetPreOrder();
	string GetPostOrder();

	TK* GetMinKey();
	TK* GetMaxKey();
	int GetCount();
	int GetHeight();

private:
	Node<TK, TD>* FindParentOfNode(const TK& key);
	Node<TK, TD>* RecursiveFindNode(const TK& key, Node<TK, TD>* ptrCurrent);
	void RecursivePush(const TK& newKey, const TD& newData, Node<TK, TD>* ptrCurrent);
	void RecursiveGetInOrder(Node<TK, TD>* ptrCurrent, stringstream& stream);
	void RecursiveGetPreOrder(Node<TK, TD>* ptrCurrent, stringstream& stream);
	void RecursiveGetPostOrder(Node<TK, TD>* ptrCurrent, stringstream& stream);
	TK* RecursiveGetMax(Node<TK, TD>* ptrCurrent);
	TK* RecursiveGetMin(Node<TK, TD>* ptuCurrent);
	int RecursiveGetHeight(Node<TK, TD>* ptrCurrent);
	void DeleteNode_NoChildren(Node<TK, TD>* deleteMe, Node<TK, TD>* parent, bool isLeftNode);
	void DeleteNode_LeftChild(Node<TK, TD>* deleteMe, Node<TK, TD>* parent, bool isLeftNode);
	void DeleteNode_RightChild(Node<TK, TD>* deleteMe, Node<TK, TD>* parent, bool isLeftNode);
	void DeleteNode_TwoChildren(Node<TK, TD>* deleteMe, Node<TK, TD>* parent, bool isLeftNode);
private:
	Node<TK, TD>* m_ptrRoot;
	int m_nodeCount;
	friend class Tester;
};
template <typename TK, typename TD>
void BinarySearchTree<TK, TD>::Push(const TK& newKey, const TD& newData)
{
	Node<TK, TD>* newNode = new Node<TK, TD>();
	newNode->key = newKey;
	newNode->data = newData;
	if (m_nodeCount == 0) { m_ptrRoot = newNode; }
	else
	{
		Node<TK, TD>* ptrCurrent = m_ptrRoot;
		while (true)
		{
			if (newNode->key < ptrCurrent->key)
			{
				if (ptrCurrent->ptrLeft == nulltpr)
				{
					ptrCurrent->ptrLeft = newNode;
					break;
				}
				else
				{
					ptrCurrent = ptrCurrent->ptrLeft;
				}
			}
			else
			{
				if (ptrCurrent->ptrRight == nullptr)
				{
					ptrCurrent->ptrRight = newNode;
					break;
				}
				else
				{
					ptrCurrent = ptrCurrent->ptrRight;
				}
			}
		}
	}
	m_nodeCount++;
}

template <typename TK, typename TD>
void BinarySearchTree<TK, TD>::Delete(const TK& key)
{
	Node<TK, TD>* deleteMe = FindNode(key);
	if (deleteMe == nullptr)
	{
		cout << key << " is not in the tree! Cannot delete" << endl;
		return;
	}
	Node<TK, TD>* parent = FindParentOfNode(key);
	bool isLeftNode = (parent != nullptr && parent->ptrLeft == deleteMe);
	if (deleteMe->ptrLeft == nullptr && deleteMe->ptrRight == nullptr) { DeleteNode_NoChildren(deleteMe, parent, isLeftNode); }
	else if (deleteMe->ptrLeft == nullptr) { DeleteNode_RightChild(deleteMe, parent, isLeftNode); }
	else if (deleteMe->ptrRight == nullptr) { DeleteNode_LeftChild(deleteMe, parent, isLeftNode); }
	else { DeleteNode_TwoChildren(deleteMe, parent, isLeftNode); }
}
template <typename TK, typename TD>
void BinarySearchTree<TK, TD>::DeleteNode_NoChildren(Node<TK, TD>* deleteMe, Node<TK, TD>* parent, bool isLeftNode)
{
	if (deleteMe == m_ptrRoot) { m_ptrRoot = nullptr; }
	else if (isLeftNode) { parent->ptrLeft = nullptr; }
	else { parent->ptrRight = nullptr; }
	delete deleteMe;
	m_nodeCount--;
}
template <typename TK, typename TD>
void BinarySearchTree<TK, TD>::DeleteNode_LeftChild(Node<TK, TD>* deleteMe, Node<TK, TD>* parent, bool isLeftNode)
{
	if (deleteMe == m_ptrRoot) { m_ptrRoot = deleteMe->ptrLeft; }
	else if (isLeftNode) { parent->ptrLeft = deleteMe->ptrLeft; }
	else { parent->ptrRight = deleteMe->ptrLeft; }
	deleteMe->ptrLeft = nullptr;
	delete deleteMe;
	m_nodeCount--;
}
template <typename TK, typename TD>
void BinarySearchTree<TK, TD>::DeleteNode_RightChild(Node<TK, TD>* deleteMe, Node<TK, TD>* parent, bool isLeftNode)
{
	if (deleteMe == m_ptrRoot) { m_ptrRoot = deleteMe->ptrRight; }
	else if (isLeftNode) { parent->ptrLeft = deleteMe->ptrRight; }
	else { parent->ptrRight = deleteMe->ptrRight; }
	deleteMe->ptrRight = nullptr;
	delete deleteMe;
	m_nodeCount--;
}
template <typename TK, typename TD>
void BinarySearchTree<TK, TD>::DeleteNode_TwoChildren(Node<TK, TD>* deleteMe, Node<TK, TD>* parent, bool isLeftNode)
{
	Node<TK, TD>* tempNode = deleteMe->ptrRight;
	Node<TK, TD>* successor = deleteMe;
	Node<TK, TD>* successorParent = deleteMe;
	while (tempNode != nullptr)
	{
		succcessorParent = successor;
		successor = tempNode;
		tempNode = tempNode->ptrLeft;
	}
	if (successor == deleteMe->ptrRight) {}
	else
	{
		successorParent->ptrLeft = successor->ptrRight;
		successor->ptrRight = deleteMe->ptrRight;
	}
	if (deleteMe == m_ptrRoot) { m_ptrRoot = successor; }
	else if (isLeftNode) { parent->ptrLeft = successor; }
	else { parent->ptrRight = successor; }
	successor->ptrLeft = deleteMe->ptrLeft;
	deleteMe->ptrLeft = nullptr;
	deleteMe->ptrRight = nullptr;
	delete deleteMe;
	m_nodeCount--;
}
template <typename TK, typename TD>
bool BinarySearchTree<TK, TD>::Contains(const TK& key)
{
	return RecursiveContains(key, m_ptrRoot);
}
template <typename TK, typename TD>
bool BinarySearchTree<TK, TD>::RecursiveContains(const TK& key, Node<TK, TD>* ptrCurrent)
{
	if (ptrCurrent == nullptr) { return false; }
	if (key == ptrCurrent->key) { return true; }
	return (RecursiveContains(key, PtrCurrent->ptrLeft) 
		|| RecursiveContains(key, ptrCurrent->ptrRight));
}
template<typename TK, typename TD>
string BinarySearchTree<TK, TD>::GetInOrder()
{
	stringstream stream;
	RecursiveGetInOrder(m_ptrRoot, stream);
	return stream.str();
}
template<typename TK, typename TD>
string BinarySearchTree<TK, TD>::GetPreOrder()
{
	stringstream stream;
	RecursiveGetPreOrder(m_ptrRoot, stream);
	return stream.str();
}
template <typename TK, typename TD>
string BinarySearchTree<TK, TD>::GetPostOrder()
{
	stringstream stream;
	RecursiveGetPostOrder(m_ptrRot, stream);
	return stream.str();
}
template <typename TK, typename TD>
TK* BinarySearchTree<TK, TD>::GetMaxKey() { return RecursiveGetMax(m_ptrRoot); }
template <typename TK, typename TD>
TK* BinarySearchTree<TK, TD>::GetMinKey() { return RecursiveGetMin(m_ptrRoot); }
template <typename TK, typename TD>
int BinarySearchTree<TK, TD>::GetCount() { return m_nodeCount; }
template <typename TK, typename TD>
Node<TK, TD>* BinarySearchTree<TK, TD>::FindNode(const TK& key)
{
	return RecursiveFindNode(key, m_ptrRot);
}
template <typename TK, typename TD>
Node<TK, TD>* BinarySearchTree<TK, TD>::FindParentOfNode(const TK& key)
{
	if (m_ptrRoot == nullptr) { return nullptr; }
	if (m_ptrRoot->key == key) { return nullptr; }
	Node<TK, TD>* ptrCurrent = m_ptrRoot;
	while (ptrCurrent != nullptr)
	{
		if (ptrCurrent->ptrLeft != nullptr && ptrCurrent->ptrLeft->key == key) { return ptrCurrent; }
		else if (ptrCurrent->ptrRight != nullptr &&
			ptrCurrent->ptrRight->key == key) {
			return ptrCurrent;
		}
		if (key < ptrCurrent->key)
		{
			ptrCurrent = ptrCurrent->ptrLeft;
		}
		else
		{
			ptrCurrent = ptrCurrent->ptrRight;
		}
	}
	return nullptr;
}
template <typename TK, typename TD>
Node<TK, TD>* BinarySearchTree<TK, TD>::RecursiveFindNode(const TK& key, Node<TK, TD>* ptrCurrent)
{
	if (m_ptrRoot == nullptr) { return nullptr; }
	if (ptrCurrent == nullptr) { return nullptr; }
	else if (ptrCurrent->key == key) { return ptrCurrent; }
	else if (ptrCurrent->key > key) { RecursiveFindNode(key, ptrCurrent->ptrLeft); }
	else if (ptrCurrent->key < key) { RecursiveFindNode(key, ptrCurrent->ptrRight); }
	else { return nullptr; }
}
template <typename TK, typename TD>
void BinarySearchTree<TK, TD>::RecursivePush(const TK& newKey, const TD& newData, Node<TK, TD>* ptrCurrent)
{
	if (newKey < ptrCurrent->key)
	{
		if (ptrCurrent->ptrLeft == nullptr)
		{
			ptrCurrent->ptrLeft = new Node<TK, TD>();
			ptrCurrent->ptrLeft->key = newKey;
			ptrCurrent->ptrLeft->data = newData;
			m_nodeCount++;
			return;
		}
		else { RecursivePush(newKey, newData, ptrCurrent->ptrLeft); }
	}
	else
	{
		if (ptrCurrent->ptrRight == nullptr)
		{
			ptrCurrent->ptrRight = new Node<TK, TD>();
			ptrCurrent->ptrRight->key = newKey;
			ptrCurrent->ptrRight->data = newData;
			m_nodeCount++;
			return;
		}
		else { RecursivePush(newKey, newData, ptrCurrent->ptrRight); }
	}
}
template <typename TK, typename TD>
void BinarySearchTree<TK, TD>::RecursiveGetInOrder(Node<TK, TD>* ptrCurrent, stringstream& stream)
{
	if (ptrCurrent == nullptr) { return; }
	else
	{
		if (ptrCurrent->ptrLeft != nullptr)
		{ RecursiveGetInOrder(ptrCurrent->ptrLeft, stream); }
		stream << ptrCurrent->key << " ";
		if (ptrCurrent->ptrRight != nullptr, stream)
		{
			RecursiveGetInOrder(ptrCurrent->ptrRight, stream);
		}
	}
	
}
template <typename TK, typename TD>
void BinarySearchTree<TK, TD>::RecursiveGetPreOrder(Node<TK, TD>* ptrCurrent, stringstream& stream)
{
	if (ptrCurrent == nullptr) { return; }
	else
	{
		stream << ptrCurrent->key << " ";
		if (ptrCurrent->ptrLeft != nullptr)
		{
			RecursiveGetPreOrder(ptrCurrent->ptrLeft, stream);
		}
		if (ptrCurrent->ptrRight != nullptr, stream)
		{
			RecursiveGetPreOrder(ptrCurrent->ptrRight, stream);
		}
	}
}

#endif // !_BINARY_SEARCH_TREE_HPP
