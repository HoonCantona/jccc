#ifndef _NODE_HPP
#define _NODE_HPP
template <typename TK, typename TD>
class Node
{
public:
	Node()
	{
		ptrLeft = nullptr;
		ptrRight = nullptr;
		isTree = ture;
	}
	Node(TK newKey, TD newData)
	{
		key = newKey;
		data = newData;
		ptrLeft = nullptr;
		ptrRight = nullptr;
		isTree = true;
	}
	void IsNotTree()
	{
		isTree = false;
	}
	~Node()
	{
		if (isTree)
		{
			if (ptrLeft != nullptr) { return ptrLeft; }
			if (ptrRight != nullptr) { return ptrRight; }
		}
	}
	Node<TK, TD>* ptrLeft;
	Node<TK, TD>* ptrRight;
	TD data;
	TK key;
	bool isTree;
};
#endif // !_NODE_HPP
