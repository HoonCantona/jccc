#ifndef LINKED_LIST_HPP
#define LINKED_LIST_HPP

#include <iostream>
#include <string>
#include <stdexcept>
using namespace std;

template <typename T>
struct Node
{
    public:
    Node();

    Node<T>* m_ptrNext;
    Node<T>* m_ptrPrev;

    T m_data;
};

template <typename T>
class LinkedList
{
    private:
    /* Member Variables */
    //! A pointer to the first item in the list
    Node<T>* m_ptrFirst;
    //! A pointer to the last item in the list
    Node<T>* m_ptrLast;
    //! The amount of items stored in the list
    int m_itemCount;

    public:
    /* Member Functions */
    LinkedList();
    ~LinkedList();

    //! Add a new item to the front of the list
    void PushFront( T newData );
    //! Add a new item to the back of the list
    void PushBack( T newData );
    //! Remove the front-most item
    void PopFront() noexcept;
    //! Remove the last item
    void PopBack() noexcept;
    //! Get the data of the front-most item
    T& GetFront();
    //! Get the data of the back-most item
    T& GetBack();
    //! Subscript operator to get an item at an arbitrary index
    T& operator[]( const int index );

    //! Clear all items out of the list
    void Clear();
    //! Returns true if the list is empty, or false otherwise
    bool IsEmpty();
    //! Returns the amonut of items stored in the list
    int Size() const;
    //! Returns whether the given index is invalid.
    bool IsInvalidIndex( int index ) const;

	void Panic(string message) const;
    void NotImplemented() const;

    friend class Tester;
};

/**
    Set the next and previous pointers to nullptr
*/
template <typename T>
Node<T>::Node()
{
    m_ptrNext = nullptr;
    m_ptrPrev = nullptr;
}

/**
    - Initialize the first and last pointers to nullptr.
    - Set the item count to 0.
*/
template <typename T>
LinkedList<T>::LinkedList() /*                                                  LinkedList */
{		//Construct LinkedList(), which includes m_ptrFirst, m_ptrLast and m_itemCount
    m_ptrFirst = nullptr;
    m_ptrLast = nullptr;
    m_itemCount = 0;		//Keep tracking the m_itemCount
}

/**
    Call the Clear function
*/
template <typename T>
LinkedList<T>::~LinkedList() /*                                                 ~LinkedList */
{
    Clear();
}

/**
    @return void

    - While the list is not empty...
        - Pop the front most item.
*/
template <typename T>
void LinkedList<T>::Clear() /*                                                  Clear */
{
    //NotImplemented();
	
	while (m_ptrFirst != nullptr)
	{
		PopFront();
	}
}

/**
    @param  T newData       New item to add to the list
    @return void

    Add a new item to the beginning of the list.

    - Create a Node<T>* pointer, and allocate new memory via it.
    - Set the new node's data to the newData.
    - Increment the item count
    - If the list is currently empty...
        - Set the first and last pointers to this new pointer.
    - Otherwise...
        - Set the first pointer's previous item to this new pointer.
        - Set the new node's next pointer to the first item.
        - Update the first pointer to point to the new item.
*/
template <typename T>
void LinkedList<T>::PushFront( T newData ) /*                                   PushFront */
{
	//NotImplemented();
	Node<T>* newNode = new Node<T>();		//creat a newNode, which includes m_ptrPrev, m_ptrNext
	newNode->m_data = newData;			//being assigned a newData at m_data in newNode
	/*newNdptr.m_ptrNext = nullptr;
	newNdptr.m_ptrPrev = nullptr;
	newNdptr->newData = newData;*/
	
	if (IsEmpty())
	{
		/*m_ptrFirst->m_ptrNext = newNode;
		m_ptrLast->m_ptrPrev = newNode;*/
		/*newNode->m_ptrPrev = m_ptrFirst;
		newNode->m_ptrNext = m_ptrLast;
		m_itemCount++;*/
		m_ptrFirst = newNode;		//m_ptrFirst, which was nullptr, points newNode's address
		m_ptrLast = newNode;		//m_ptrLast, which was nullput, points newNode's address
		m_itemCount++;
	}
	else
	{
		/*m_ptrFirst->m_ptrPrev = newNode;
		newNode->m_ptrNext = m_ptrFirst;
		newNode->m_ptrPrev = nullptr;*/
		m_ptrFirst->m_ptrPrev = newNode;		//the old node's m_ptrPrev, which was nullptr at the front most, points newNode's address
		newNode->m_ptrNext = m_ptrFirst;		//the new node's m_ptrNext at the front most newly points the old node's address
		m_ptrFirst = newNode;		//the front most node is the newNode
		m_itemCount++;
		/*m_ptrLast->m_ptrNext = newNode;
		newNode->m_ptrPrev = m_ptrLast;
		m_ptrLast = newNode;
		m_itemCount++;*/
	}
}

/**
    @param  T newData       New item to add to the list
    @return void

    Add a new item to the end of the list.

    - Create a Node<T>* pointer, and allocate new memory via it.
    - Set the new node's data to the newData.
    - Increment the item count
    - If the list is currently empty...
        - Set the first and last pointers to this new pointer.
    - Otherwise...
        - Set the last pointer's next item to this new pointer.
        - Set the new node's previous pointer to the last item.
        - Update the last pointer to point to the new item.
*/
template <typename T>
void LinkedList<T>::PushBack( T newData ) /*                                    PushBack */
{
   // NotImplemented();
	
	Node<T>* newNode = new Node<T>();
	newNode->m_data = newData;
	
	if (IsEmpty())
	{
		m_ptrFirst = newNode;
		m_ptrLast = newNode;
		m_itemCount++;
	}
	else
	{
		/*newNode->m_ptrNext = nullptr;
		m_ptrLast->m_ptrNext = newNode;
		newNode->m_ptrPrev = m_ptrLast;*/
		m_ptrLast->m_ptrNext = newNode;		//m_ptrNext, which was pointed by the m_ptrLast that pointed the old node at last, is now pointing the newNode's address
		newNode->m_ptrPrev = m_ptrLast;
		m_ptrLast = newNode;
		m_itemCount++;
	}
}

/**
    @return void

    Remove the first-most item in the list, and update the m_ptrFirst to point
    to the new first item.

    - If the list is Empty... do nothing
    - Else, if there is only one item in the list:
        - Delete (either the first or last) node in the list (only one).
        - Set the first and last pointer to nullptr
        - Decrement the item count
    - Else:
        - Create a pointer to point to the second item.
        - Set the second item's previous pointer to nullptr.
        - Delete the first item
        - Set the m_ptrFirst pointer to that second item.
        - Decrement the item count
*/
template <typename T>
void LinkedList<T>::PopFront() noexcept /*                                      PopFront */
{
    //NotImplemented();
	if (IsEmpty())
	{

	}
	else if (m_itemCount == 1)
	{
		delete m_ptrFirst;		//delete the existing node
		m_ptrFirst = nullptr;		
		m_ptrLast = nullptr;
		m_itemCount--;
	}
	else
	{
		/*Node<T>* temp = new Node<T>();
		m_ptrFirst->m_ptrNext = temp;
		temp->m_ptrPrev = nullptr;
		delete m_ptrFirst;
		m_ptrFirst = temp;
		m_itemCount--;*/
		m_ptrFirst = m_ptrFirst->m_ptrNext;		//m_ptrFirst that points the front most node is now pointing the followig node's address pointed by the m_ptrNext in the front most node
		delete m_ptrFirst->m_ptrPrev;		//delete the front most node
		m_ptrFirst->m_ptrPrev = nullptr;		//prevent memory leak
		m_itemCount--;
	}
}

/**
    @return void

    Remove the last-most item in the list, and update the m_ptrLast to point
    to the new last item.

    - If the list is Empty... do nothing
    - Else, if there is only one item in the list:
        - Delete (either the first or last) node in the list (only one).
        - Set the first and last pointer to nullptr
        - Decrement the item count
    - Else:
        - Create a pointer to point to the second-to-last item.
        - Set the second-to-last item's next pointer to nullptr.
        - Delete the last item
        - Set the m_ptrLast pointer to that second-to-last item.
        - Decrement the item count
*/
template <typename T>
void LinkedList<T>::PopBack() noexcept /*                                       PopBack */
{
    //NotImplemented();
	if (IsEmpty())
	{

	}
	else if (m_itemCount == 1)
	{
		delete m_ptrFirst;
		m_ptrFirst = nullptr;
		m_ptrLast = nullptr;
		m_itemCount--;
	}
	else
	{
		/*Node<T>* temp = new Node<T>();
		temp = m_ptrLast->m_ptrNext;*/
		m_ptrLast = m_ptrLast->m_ptrPrev;
		delete m_ptrLast->m_ptrNext;
		m_ptrLast->m_ptrNext = nullptr;
		m_itemCount--;
	}
}

/**
    @return T&          The front item in the list is returned

    Error checks:
    - If the list is empty, throw an exception.

    Otherwise, return the data that belongs to the front pointer (m_ptrFirst).
*/
template <typename T>
T& LinkedList<T>::GetFront() /*                                                 GetFront */
{
    //NotImplemented();
    //return m_ptrFirst->m_data; // TEMPORARY
	//return T & ;
	if (IsEmpty())
	{
		throw runtime_error("No available for GetFront");
	}
	else
	{
		return m_ptrFirst->m_data;		//return the data in the front most node
	}
}

/**
    @return T&          The back item in the list is returned

    Error checks:
    - If the list is empty, throw an exception.

    Otherwise, return the data that belongs to the last pointer (m_ptrLast).
*/
template <typename T>
T& LinkedList<T>::GetBack() /*                                                  GetBack */
{
    //NotImplemented();
    //return m_ptrFirst->m_data; // TEMPORARY
	if (IsEmpty())
	{
		throw runtime_error("Can't GetBack, it's empty");
	}
	else
	{
		return m_ptrLast->m_data;		//return the data at the last node
	}
}

/**
    @param  int index   The position of the item to return.
    @return T&          Returns the item at the given index.

    Error checks:
    - If the list is empty, throw an exception.
    - If the index is invalid, throw an exception.

    Functionality:
    You'll need to traverse through the list, starting at the beginning and moving forward, one-by-one.

    - Create a Node<T>* "walker" pointer to walk through the items. Start it at the m_ptrFirst position.
    - Make a loop that will loop 'index' amount of times. Within the loop...
        - Move your walking pointer forward by one (current = current->m_ptrNext)
    - Once done, return the data of the item (access via the walker pointer.)
*/
template <typename T>
T& LinkedList<T>::operator[]( const int index ) /*                              operator[] */
{
    //NotImplemented();
    //return m_ptrFirst->m_data; // TEMPORARY
	if (IsEmpty())
	{
		throw out_of_range("Cannot et an item from an empty list");
	}
	else if (IsInvalidIndex(index))
	{
		throw out_of_range("Invalid index passed into [	]");
	}

	int counter = 0;
	Node<T>* current = m_ptrFirst;		//creating a current, which moving around the nodes that have data
	while (current != nullptr && counter < index)
	{
		current = current->m_ptrNext;
		counter++;
	}
	return current->m_data;		//return the data that is located in the index number that is assigned parameter
}

/**
    @return bool    Return true if there are no items stored in the list, and false otherwise.
*/
template <typename T>
bool LinkedList<T>::IsEmpty() /*                                                IsEmpty */
{
    //NotImplemented();
    //return false; // TEMPORARY
	return m_itemCount == 0;
}

/**
    @return int     The amount of items stored in the List. Use m_itemCount here.
*/
template <typename T>
int LinkedList<T>::Size() const/*                                                    Size */
{
    //NotImplemented();
    //return -1; // TEMPORARY
	
	return m_itemCount;
}

/**
    @param int index    The index to look at.
    @return bool        true if invalid index (less than 0 or >= m_arraySize),
                        or false if not invalid.
*/
//! Check to see if a given index is invalid (i.e., negative).
template <typename T>
bool LinkedList<T>::IsInvalidIndex( int index ) const /*                               IsInvalidIndex */
{
    //NotImplemented();
    //return false; // TEMPORARY
	if ( index < 0 || index >= m_itemCount)
	{
		return true;
	}
	else
	{
		return false;
	}

}


/* ****************************************************************************/
/* ************************************************* FUNCTION TO THROW ERRORS */
/* ****************************************************************************/

//! Call this function if something terrible goes wrong.
template <typename T>
void LinkedList<T>::Panic(string message) const /*                                   Panic */
{
	throw runtime_error(message);
}

//! Marks when a function hasn't been implemented yet.
template <typename T>
void LinkedList<T>::NotImplemented() const /*                                   NotImplemented */
{
    throw runtime_error( "Function not implemented yet!" );
}

#endif
