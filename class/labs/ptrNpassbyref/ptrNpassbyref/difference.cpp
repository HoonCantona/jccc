#include <iostream>
using namespace std;

void psByVal(int val);
void psByRef(int & ref);
void psByPtr(int* ptr);

int main()
{
	int a = 10;
	int b = 20;
	int* c = &b;
	
	psByVal(a);
	psByRef(a);
	cout << endl;
	psByVal(b);
	psByRef(b);
	cout << endl;
	psByPtr(c);
	*c = 30;
	psByPtr(c);
	cout << endl;
	return 0;
}

void psByVal(int val)
{
	cout << "Pass By Value" << endl;
	cout << "Before: " << val << endl;
	val = 11;
	cout << "After1: " << val << endl;
	val = 12;
	cout << "After2: " << val << endl;

}
void psByRef(int & ref)
{
	cout << "Pass By Ref" << endl;
	cout << "Before: " << ref << "\t" << endl;
	ref = 21;
	cout << "After1: " << ref << endl;
	ref = 22;
	cout << "After2: " << ref << endl;

}
void psByPtr(int* ptr)
{
	cout << "Pointers" << endl;
	cout << "Before: " << ptr << endl;
	cout << "After1: " << *ptr << endl;
	*ptr = 31;
	cout << "After2: " << ptr << "\t" << *ptr << endl;
}