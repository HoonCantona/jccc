#ifndef LINKED_LIST_HPP
#define LINKED_LIST_HPP

#include <iostream>
#include <string>
#include <stdexcept>
using namespace std;

template <typename T>
struct Node
{
	Node();
	Node<T>* pNext;
	Node<T>* pPrev;
	T data;
};
template <typename T>
class LinkedList
{
public:
	LinkedList();
	~LinkedList();
	void Clear();
	int Size();
	bool IsEmpty();
	bool IsInvaildIndex(const int index) const;
	bool Panic(string mess) const;
	void PopFront();
	void PopBack();
	void PushFront(T nData);
	void PushBack(T nData);
	T& GetFront();
	T& GetBack();
	T& operator[](const int index) const;
private:
	Node<T>* pFirst;
	Node<T>* pLast;
	int itemCount;
};
template <typename T>
Node<T>::Node()
{
	pNext = nullptr;
	pPrev = nullptr;
}
template <typename T>
LinkedList<T>::LinkedList()
{
	pFirst = nullptr;
	pLast = nullptr;
	itemCount = 0;
}
template <typename T>
LinkedList<T>::~LinkedList()
{
	Clear();
}
template <typename T>
void LinkedList<T>::Clear()
{
	while (pFirst != nullptr)
	{
		PopFront();
	}
}
template <typename T>
int LinkedList<T>::Size()
{
	return itemCount;
}
template <typename T>
bool LinkedList<T>::IsEmpty()
{
	return itemCount == 0;
}
template <typename T>
bool LinkedList<T>::IsInvaildIndex(const int index) const
{
	return (index < 0 || index >= itemCount);
}
template <typename T>
bool LinkedList<T>::Panic(string mess) const
{
	throw runtime_error(mess);
}
template <typename T>
void LinkedList<T>::PopFront()
{
	if (IsEmpty())
	{

	}
	else if (itemCount == 1)	//or pFirst == pLast
	{
		delete pFirst;  //delete the node, which is pointed by pFirst
		pFirst = nullptr;
		pLast = nullptr;
		itemCount--;
	}
	else
	{
		pFirst = pFirst->pNext;		//set pFirst as the following node that is pointed by pFirst's pNext
		delete pFirst->pPrev;		//delete the frond node that is pointed by pFirst's pPrev
		pFirst->pPrev = nullptr;	//set pFirst's pPrev as a free memory
		itemCount--;
	}
}
template <typename T>
void LinkedList<T>::PopBack()
{
	if (IsEmpty())
	{

	}
	else if (pLast == pFirst)
	{
		delete pLast;		
		pFirst = nullptr;
		pLast = nullptr;
		itemCount--;
	}
	else
	{
		pLast = pLast->pPrev;		//set pLast as the previous node that is pointed by pLast's pPrev
		delete pLast->pNext;
		pLast->pNext = nullptr;
		itemCount--;
	}
}
template <typename T>
void LinkedList<T>::PushFront(T nData)
{
	Node<T>* nNode = new Node();
	nNode->data = nData;
	if (IsEmpty())
	{
		pFirst = nNode;
		pLast = nNode;
	}
	else
	{
		pFirst->pPrev = nNode;
		nNode->pNext = pFirst;
		pFirst = nNode;
	}
	itemCount++;
}
template <typename T>
void LinkedList<T>::PushBack(T nData)
{
	Node<T>* nNode = new Node();
	nNode->data = nData;
	if (IsEmpty())
	{
		pFirst = nNode;
		pLast = nNode;
	}
	else
	{
		pLast->pNext = nNode;
		nNode->pPrev = pLast;
		pLast = nNode;
	}
	itemCount++;
}
template <typename T>
T& LinkedList<T>::GetFront()
{
	return pFirst->data;
}
template <typename T>
T& LinkedList<T>::GetBack()
{
	return pLast->data;
}
template <typename T>
T& LinkedList<T>::operator[](const int index) const
{
	Node<T>* curr = new Node();
	if (IsEmpty())
	{
		throw out_of_range("Not Available");
	}
	else if (IsInvaildIndex(index))
	{
		throw out_of_range("Not Available");
	}
	for (int counter = 0; curr != nullptr && count < index; counter++)
	{
		curr = curr->pNext;
	}
	return curr->data;
}
#endif