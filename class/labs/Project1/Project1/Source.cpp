#ifndef LINKED_LIST_HPP
#define LINKED_LIST_HPP

#include <iostream>
#include <string>
#include <stdexcept>
using namespace std;

template <typename T>
struct Node
{
public:
	Node();
	Node<T>* ptrNext;
	Node<T>* ptrPrev;
	T data;
};

template <typename T>
class LinkedList
{
public:
	LinkedList();
	~LinkedList();
	void PushFront(T newdata);
	void PushBack(T newdata);
	void PopFront();
	void PopBack();
	void Clear();
	void IsEmpty();
	int Size() const;
	bool IsInvalidIndex(int index) const;
	T& GetFront();
	T& GetBack();
	T& operator[](const int index);
	void Panic(string mess) const;

private:
	Node<T>* ptrFirst;
	Node<T>* ptrLast;
	int itemCount;
};

template <typename T>
Node<T>::Node()
{
	ptrNext = nullptr;
	ptrPrev = nullptr;
}

template <typename T>
LinkedList<T>::LinkedList()
{
	ptrFirst = nullptr;
	ptrLast = nullptr;
	itemCount = 0;
}

template <typename T>
LinkedList<T>::~LinkedList()
{
	Clear();
}
template <typename T>
void LinkedList<T>::Clear()
{
	while (ptrFirst != nullptr)
	{
		PopFront();
	}
}
template <typename T>
void LinkedList<T>::IsEmpty()
{
	return itemCount == 0;
}
template <typename T>
int LinkedList<T>::Size() const
{
	return itemCount;
}
template <typename T>
bool LinkedList<T>::IsInvalidIndex(int index) const
{
	return (index < 0 || index >= itemCount);
}
template <typename T>
void LinkedList<T>::Panic(string mess) const
{
	throw runtime_error(mess);
}
template <typename T>
void LinkedList<T>::PushFront(T newdata)
{
	Node<T>* newNode = new Node<T>();
	newNode->data = newdata;
	if (IsEmpty())
	{
		ptrFirst = newNode;
		ptrLast = newNode;
		itemCount++;
	}
	else
	{
		ptrFirst->ptrPrev = newNode;
		newNode->ptrNext = ptrFirst;
		ptrFirst = newNode;
		itemCount++;
	}
}
template <typename T>
void LinkedList<T>::PushBack(T newdata)
{
	Node<T>* newNode = new Node<T>();
	newNode->data = newdata;
	if (IsEmpty())
	{
		ptrFirst = newNode;
		ptrLast = newNode;
		itemCount++;
	}
	else
	{
		ptrLast->ptrNext = newNode;
		newNode->ptrPrev = ptrLast;
		ptrLast = newNode;
		itemCount++;
	}
}
template <typename T>
void LinkedList<T>::PopFront()
{
	if (IsEmpty())
	{

	}
	else if (itemCount == 1)
	{
		delete ptrFirst;
		ptrFirst = nullptr;
		ptrLast = nullptr;
		itemCount--;
	}
	else
	{
		ptrFirst = ptrFirst->ptrNext;
		delete ptrFirst->ptrPrev;
		ptrFirst->ptrPrev = nullptr;
		itemCount--;
	}
}
template <typename T>
void LinkedList<T>::PopBack()
{
	if (IsEmpty())
	{

	}
	else if (itemCount == 1)
	{
		delete ptrLast;
		ptrLast = nullptr;
		ptrFirst = nullptr;
		itemCount--;
	}
	else
	{
		ptrLast = ptrLast->ptrPrev;
		delete ptrLast->ptrNext;
		ptrLast->ptrNext = nullptr;
		itemCount--;
	}
}
template <typename T>
T& LinkedList<T>::GetFront()
{
	if (IsEmpty())
	{
		throw runtime_error("Not Available");
	}
	else
	{
		return ptrFirst->data;
	}
}
template <typename T>
T& LinkedList<T>::GetBack()
{
	if (IsEmpty())
	{
		throw runtime_error("Not Available");
	}
	else
	{
		return ptrLast->data;
	}
}
template <typename T>
T& LinkedList<T>::operator[](const int index)
{
	if (IsEmpty())
	{
		throw out_of_range("Not Available");
	}
	else if(IsInvalidIndex(index))
	{
		throw out_of_range("Not Available");
	}
	int count = 0;
	Node<T>* curr = ptrFirst;
	while (curr != nullptr && count < index)
	{
		curr = curr->ptrNext;
		count++;
	}
	return curr->data;
}
#endif // !LINKED_LIST_HPP
