#ifndef LINKED_LIST_HPP
#define LINKED_LIST_HPP

#include <iostream>
#include <string>
#include <stdexcept>
using namespace std;

template <typename T>
struct Node
{
public:
	Node();
	Node<T>* pNext;
	Node<T>* pPrev;
	T data;
};
template <typename T>
class LinkedList
{
public:
	LinkedList();
	~LinkedList();
	void Panic(string mess) const;
	void Clear();
	void IsEmpty();
	int Size() const;
	bool IsInvalidIndex(int index) const;
	T& GetFront();
	T& GetBack();
	T& operator[](const int index);
	void PushFront(T nData);
	void PushBack(T nData);
	void PopFront();
	void PopBack();
private:
	Node<T>* pFirst;
	Node<T>* pLast;
	int itemCount;
};
template <typename T>
Node<T>::Node()
{
	pNext = nullptr;
	pPrev = nullptr;
}
template <typename T>
LinkedList<T>::LinkedList()
{
	pFirst = nullptr;
	pLast = nullptr;
	itemCount = 0;
}

template <typename T>
LinkedList<T>::~LinkedList()
{
	Clear();
}
template <typename T>
void LinkedList<T>::Panic(string mess) const
{
	throw runtime_error(mess);
}
template <typename T>
void LinkedList<T>::Clear()
{
	while (pFirst != nullptr)
	{
		PopFront();
	}
}
template <typename T>
void LinkedList<T>::IsEmpty()
{
	return itemCount == 0;
}
template <typename T>
int LinkedList<T>::Size() const
{
	return itemCount;
}
template <typename T>
bool LinkedList<T>::IsInvalidIndex(int index) const
{
	return (index < 0 || index >= itemCount);
}
template <typename T>
void LinkedList<T>::PushFront(T nData)
{
	Node<T>* nNode = new Node<T>();
	nNode->data = nData;
	if (IsEmpty())
	{
		pFirst = nNode;
		pLast = nNode;
	}
	else
	{
		pFirst->pPrev = nNode;
		nNode->pNext = pFirst;
		nFirst = nNode;
	}
	itemCount++;
}
template <typename T>
void LinkedList<T>::PushBack(T nData)
{
	Node<T>* nNode = new Node<T>();
	nNode->data = nData;
	if (IsEmpty())
	{
		pFirst = nNode;
		pLast = nNode;
	}
	else
	{
		pLast->pNext = nNode;
		nNode->pPrev = pLast;
		pLast = nNode;
	}
	itemCount++;
}
template <typename T>
void LinkedList<T>::PopFront()
{
	if(IsEmpty()){}
	else if (pFirst == pLast)
	{
		delete pFirst;
		pFirst = nullptr;
		pLast = nullptr;
	}
	else
	{
		pFirst = pFirst->pNext;
		delete pFirst->pPrev;
		pFirst->pPrev = nullptr;
	}
	itemCount--;
}
template <typename T>
void LinkedList<T>::PopBack()
{
	if (IsEmpty()) {}
	else if (pFirst == pLast)
	{
		delete pLast;
		pFirst = nullptr;
		pLast = nullptr;
	}
	else
	{
		pLast = pLast->pPrev;
		delete pLast->pNext;
		pLast->pNext = nullptr;
	}
	itemCount--;
}
template <typename T>
T& LinkedList<T>::GetFront()
{
	if (IsEmpty())
	{
		throw runtime_error("Not Available");
	}
	else
	{
		return pFirst->data;
	}
}
template <typename T>
T& LinkedList<T>::GetBack()
{
	if (IsEmpty())
	{
		throw runtime_error("Not Available");
	}
	else
	{
		return pLast->data;
	}
}
template <typename T>
T& LinkedList<T>::operator[](const int index)
{
	if (IsEmpty())
	{
		throw out_of_range("Not Available");
	}
	else if (IsInvalidIndex(index))
	{
		throw out_of_range("Not Available")
	}
	Node<T>* curr = pFirst;
	for (int i = 0; curr != nullptr && i < index; i++)
	{
		curr = curr->pNext;
	}
	return curr->data;
}
#endif

/*
Dynamic Array
int * arr = new int[100];
or
int * arr;
arr = new int[100];
for (int i = 0; i < 100; i++)
{
	arr[i] = i;
}
delete [] arr;

Dynamic variable
Node* n = new Node;
or
Node * n;
n = new Node;
delete n;
n = nullptr;
*/