#ifndef _FUNCTIONS_HPP      // protect against duplicate code error
#define _FUNCTIONS_HPP      // protect against duplicate code error

#include <vector>           // Use the vector class
using namespace std;

// FUNCTION DECLARATIONS (Do not modify)

/***********************************************/
/** Function Set 1 ****************************/

void GetClassList(vector<string>& classList);
void RemoveLastItem(vector<string>& classList);
void ClearList(vector<string>& classList);
string GetFirst(vector<string>& classList);
string GetLast(vector<string>& classList);
string GetItem(vector<string>& classList, int index);
string GetAllItems(const vector<string>& classList);

/***********************************************/
/** Program and Test runners ******************/

void Program();
void RunTests();

/***********************************************/
/** Utilities *********************************/

string B2S( bool val );
void ClearScreen();
void Pause();

/***********************************************/
/** Test functions ****************************/

void Test_Set1();
void Test_Set2();
void Test_Set3();
void Test_Set4();
void Test_Set5();
void Test_Set6();
void Test_Set7();

const int headerWidth = 70;
const int pfWidth = 10;

#endif                      // protect against duplicate code error
