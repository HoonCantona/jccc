#include <iostream>
#include <vector>
#include <string>

using namespace std;
char* convert(const string& s);
char* recurChar(char ch, int n);

int main()
{
	string input;
	cout << "Enter a string: ";
	cin >> input;
	int n = input.size() + 1;
	cout << "String: " << input << endl;
	cout << "Size of the string: " << n << endl;
	/*vector<string> inputStr = input;
	cout << inputStr.begin() << "\t" << inputStr.end() << endl;*/
	char* convertedStr = new char[n];
	convertedStr = convert(input);
	cout << "Char: " << convertedStr << endl;
	for (int i = 0; i < n-1; i++)
	{
		cout << convertedStr[i] << endl;
	}
	recurChar(convertedStr);
	cout << "End the practice" << endl;

	
	return 0;
}

char* convert(const string& s)
{
	char* conv = new char[s.size() + 1];
	strcpy(conv, s.c_str());
	return conv;
}

char* recurChar(vector<char> ch, int n)
{
	vector<char> recurred(ch.size()*n);
	for (int i = 1; i <= n; i++)
	{
		recurred.push_back(ch);
	}
	return recurred;
}