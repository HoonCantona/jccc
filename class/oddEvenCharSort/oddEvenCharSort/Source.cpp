#include <cmath>
#include <cstdio>
#include <vector>
#include <iostream>
#include <algorithm>
using namespace std;


int main() {
	/* Enter your code here. Read input from STDIN. Print output to STDOUT */

	int testNum;
	string s;
	string oddChar;
	string evenChar;

	cout << "How many tests do you want to conduct?: ";
	cin >> testNum;
	for (int j = 0; j < testNum; j++) {
		cout << "Enter a word you want to sort: ";
		cin >> s;
		int i = 0;
		while (i < s.size()) {
			if (i % 2 == 0) {
				oddChar.push_back(s[i]);
			}
			else {
				evenChar.push_back(s[i]);
			}
			i++;
		}
		cout << oddChar << " " << evenChar << endl;
		oddChar.clear();
		evenChar.clear();
	}


	return 0;
}