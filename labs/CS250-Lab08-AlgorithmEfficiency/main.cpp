#include <iostream>
#include <string>
#include <cstdlib>
#include <ctime>
#include <vector>
using namespace std;

#include "Timer.hpp"
#include "Menu.hpp"
#include "EfficiencyExperiment.hpp"

void MainMenu();
void SearchingEfficiency();
void FibonacciEfficiency();

int main()
{
//    srand( time( NULL ) );    // Don't want this.

    MainMenu();

    return 0;
}

void MainMenu()
{
    bool done = false;
    while ( !done )
    {
        Menu::ClearScreen();
        Menu::Header( "CS 250 Lab 8 - Main Menu" );

        int choice = Menu::ShowIntMenuWithPrompt( { "Searching efficiency", "Fibonacci efficiency", "Quit" } );
        switch( choice )
        {
            case 1:     SearchingEfficiency();      break;
            case 2:     FibonacciEfficiency();      break;
            case 3:     done = true;                break;
        }
    }
}

void SearchingEfficiency()
{
    Menu::ClearScreen();
    Menu::Header( "Searching Efficiency" );

    EfficiencyExperiment ex;
    Timer timer;

    bool done = false;
    while ( !done )
    {
        int foundIndex = -1;
        int arraySize = Menu::GetIntChoice( "[SIZE]     Enter the size of the array size:" );

        cout << " [SEARCH]" << endl;
        int type = Menu::ShowIntMenuWithPrompt( { "Linear search", "User's search", "Binary Search (Requires sorted list)" } );

        int findMe = Menu::GetIntChoice( "[FIND]    Enter an INTEGER value to find:" );

        cout << " [MESSAGES]" << endl;
        bool debugOut = ( Menu::ShowIntMenuWithPrompt( { "Show status messages", "No status messages" } ) == 1 ) ? true : false;

        if ( type == 1 )
        {
			cout << " Building array... ";
			timer.Start();
            ex.SetupVector( arraySize );
			cout << " Done.\t Time elapsed: " << timer.GetElapsedMilliseconds() << " milliseconds to build array." << endl << endl;
            cout << " Searching with LINEAR SEARCH... ";
            timer.Start();
            foundIndex = ex.LinearSearch( findMe, debugOut );
        }
        else if ( type == 2 )
        {
			cout << " Building array... ";
			timer.Start();
            ex.SetupVector( arraySize );
			cout << " Done.\t Time elapsed: " << timer.GetElapsedMilliseconds() << " milliseconds to build array." << endl << endl;
            cout << " Searching with STUDENT SEARCH... ";
            timer.Start();
            foundIndex = ex.StudentSearch( findMe, debugOut );
        }
        else if ( type == 3 )
        {
			cout << " Building array... ";
			timer.Start();
            ex.SetupVector( arraySize, true );
			cout << " Done.\t Time elapsed: " << timer.GetElapsedMilliseconds() << " milliseconds to build array." << endl << endl;
            cout << " Searching (pre-sorted list) with BINARY SEARCH... ";
            timer.Start();
            foundIndex = ex.BinarySearch( findMe, debugOut );

        }

        cout << " Done.\t Time elapsed: " << timer.GetElapsedMilliseconds() << " milliseconds to search array." << endl << endl;

        if ( foundIndex == -1 )
            cout << " Result: " << findMe << " was not found." << endl;
        else
            cout << " Result: " << findMe << " was found at index " << foundIndex << "." << endl;

        cout << endl << " [AGAIN?]" << endl;
        int another = Menu::ShowIntMenuWithPrompt( { "Go back", "Do another" } );
        done = ( another == 1 );
        Menu::DrawHorizontalBar( 80 );
    }
}

void FibonacciEfficiency()
{
    Menu::ClearScreen();
    Menu::Header( "Fibonacci Efficiency" );

    EfficiencyExperiment ex;
    Timer timer;

    bool done = false;
    while( !done )
    {
        int n = Menu::GetIntChoice( "[N]    Generate the nth fibonacci number:" );
        int fib = -1;

        cout << " [TYPE]" << endl;
        int type = Menu::ShowIntMenuWithPrompt( { "Iterative method", "Recursive method" } );

        if ( type == 1 )
        {
            cout << " Generating the " << n << "th Fibonacci number ITERATIVELY... " << endl;
            timer.Start();
            fib = ex.Fibonacci_Iter( n );
        }
        else if ( type == 2 )
        {
            cout << " Generating the " << n << "th Fibonacci number RECURISVELY... " << endl;
			timer.Start();
            fib = ex.Fibonacci_Rec( n );
        }

        cout << endl << " Done.\t Time elapsed: " << timer.GetElapsedMilliseconds() << " milliseconds" << endl;
        cout << " Result: " << fib << " is the " << n << "th Fibonacci number." << endl;

        cout << endl << " [AGAIN?]" << endl;
        int another = Menu::ShowIntMenuWithPrompt( { "Go back", "Do another" } );
        done = ( another == 1 );
        Menu::DrawHorizontalBar( 80 );
    }
}
