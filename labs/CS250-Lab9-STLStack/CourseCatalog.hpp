#ifndef _COURSE_CATALOG_HPP
#define _COURSE_CATALOG_HPP

#include <iostream>
#include <fstream>
#include <vector>
using namespace std;

#include "Course.hpp"

class CourseCatalog
{
    public:
    //! Initializes the course catalog
    CourseCatalog();

    //! Runs the course catalog program
    void Run();

    private:

    //! Loads all courses from a text file
    void LoadCourses();

    //! Displays list of all courses
    void ViewCourses();

    //! User enters course, list of prereqs are displayed
    void ViewPrereqs();

    //! Finds a course by its code and returns the course
    Course FindCourse( const string& code );

    //! The vector of courses
    vector<Course> m_courses;
};

#endif
